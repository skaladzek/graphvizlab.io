---
defaults: []
flags:
- write
minimums: []
name: head_lp
types:
- point
used_by: E
---
Position of an edge's head label, [in points](#points). The position indicates the center of the label.
